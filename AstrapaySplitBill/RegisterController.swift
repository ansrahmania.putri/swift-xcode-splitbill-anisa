//
//  RegisterController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 06/12/22.
//

import UIKit
import Alamofire

class RegisterController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var labelBuatAkun: UILabel!
    @IBOutlet weak var labelNoHP: UILabel!
    
    @IBOutlet weak var tfNoHP: UITextField!
    @IBOutlet weak var labelPass: UILabel!
    @IBOutlet weak var tfPass: UITextField!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var btnRegister: UIButton!
    
    @IBOutlet weak var labelRegexUsername: UILabel!
    @IBOutlet weak var labelRegexPass: UILabel!
    @IBOutlet weak var labelRegexEmail: UILabel!
    @IBOutlet weak var labelRegexRetypePass: UILabel!
    
    @IBOutlet weak var lblRetypePass: UILabel!
    @IBOutlet weak var btnLoginReg: UIButton!
    
    @IBOutlet weak var tfRetypePass: UITextField!
    
    @IBOutlet weak var lblNamaLengkap: UILabel!
    @IBOutlet weak var tfNamaLengkap: UITextField!
    @IBOutlet weak var labelRegexNamaLengkap: UILabel!
    
    
    @IBOutlet weak var lblTglLahir: UILabel!
    @IBOutlet weak var tfTglLahir: UITextField!
    @IBOutlet weak var labelRegexTglLahir: UILabel!
    
    var iconClick = false
    
    let imageIcon = UIImageView()
    let imageIcon2 = UIImageView()
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = true
        
        labelBuatAkun.text = "Buat Akun"
        
        tfNoHP.delegate = self
        tfPass.delegate = self
        tfEmail.delegate = self
        tfRetypePass.delegate = self
        tfNamaLengkap.delegate = self
        tfTglLahir.delegate = self
        
        
        labelNoHP.text = "Nomor Handphone"
        labelPass.text = "Password"
        labelEmail.text = "Email"
        lblRetypePass.text = "Ketik Ulang Password"
        lblNamaLengkap.text = "Nama Lengkap"
        lblTglLahir.text = "Tanggal Lahir"
    
        
        btnRegister.addTarget(self, action: #selector(btnRegisterTapped), for: .touchUpInside)
        btnLoginReg.addTarget(self, action: #selector(btnLoginRegTapped), for: .touchUpInside)
        
        labelRegexPass.isHidden = true
        labelRegexUsername.isHidden = true
        labelRegexEmail.isHidden = true
        labelRegexRetypePass.isHidden = true
        labelRegexNamaLengkap.isHidden = true
        labelRegexTglLahir.isHidden = true
        
        btnRegister.isEnabled = false
        
        imageIcon.image = UIImage(named: "hide")
        imageIcon2.image = UIImage(named: "hide")
        
        let contentView = UIView()
        let contentView2 = UIView()
        contentView.addSubview(imageIcon)
        contentView2.addSubview(imageIcon2)
        
        contentView.frame = CGRect(x: 0, y: 0, width: UIImage(named: "hide")!.size.width, height: UIImage(named: "hide")!.size.height)
        contentView2.frame = CGRect(x: 0, y: 0, width: UIImage(named: "hide")!.size.width, height: UIImage(named: "hide")!.size.height)
        
        imageIcon.frame = CGRect(x: -10, y: 0, width: UIImage(named: "hide")!.size.width, height: UIImage(named: "hide")!.size.height)
        imageIcon2.frame = CGRect(x: -10, y: 0, width: UIImage(named: "hide")!.size.width, height: UIImage(named: "hide")!.size.height)
        
        tfPass.rightView = contentView
        tfPass.rightViewMode = .always
        
        tfRetypePass.rightView = contentView2
        tfRetypePass.rightViewMode = .always
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageIcon.isUserInteractionEnabled = true
        imageIcon.addGestureRecognizer(tapGestureRecognizer)
        let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(imageTapped2(tapGestureRecognizer:)))
        imageIcon2.isUserInteractionEnabled = true
        imageIcon2.addGestureRecognizer(tapGestureRecognizer2)
        
        showDatePicker()
    
    }
    
    func showDatePicker(){
    datePicker.datePickerMode = .date
    datePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels

     let toolbar = UIToolbar();
     toolbar.sizeToFit()
     let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
    let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

   toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

    tfTglLahir.inputAccessoryView = toolbar
    tfTglLahir.inputView = datePicker

   }

    @objc func donedatePicker(){

     let formatter = DateFormatter()
     formatter.dateFormat = "yyyy-MM-dd"
     tfTglLahir.text = formatter.string(from: datePicker.date)
     self.view.endEditing(true)
   }

   @objc func cancelDatePicker(){
      self.view.endEditing(true)
       
   }
    
    @objc func imageTapped(tapGestureRecognizer:UITapGestureRecognizer) {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        if iconClick {
            iconClick = false
            tappedImage.image = UIImage(named: "view")
            tfPass.isSecureTextEntry = false
        } else {
            iconClick = true
            tappedImage.image = UIImage(named: "hide")
            tfPass.isSecureTextEntry = true
        }
    }
    
    @objc func imageTapped2(tapGestureRecognizer:UITapGestureRecognizer) {
        let tappedImage2 = tapGestureRecognizer.view as! UIImageView
        
        if iconClick {
            iconClick = false
            tappedImage2.image = UIImage(named: "view")
            tfRetypePass.isSecureTextEntry = false
        } else {
            iconClick = true
            tappedImage2.image = UIImage(named: "hide")
            tfRetypePass.isSecureTextEntry = true
        }
    }
    
    
    @objc func btnLoginRegTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnRegisterTapped() {
        let param = [
                    "username": tfNoHP.text,
                    "password": tfPass.text,
                    "email": tfEmail.text,
                    "userDet" : [
                        "fullName": tfNamaLengkap.text,
                        "birthDate": tfTglLahir.text]] as [String : Any]
        
        AF.request("http://localhost:8080/register", method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    let itemObject = response.value as? [String : Any]
                    print(itemObject)
                    let success = itemObject? ["success"] as? Bool
                    let message = itemObject? ["message"] as? String
                
                    if success! {
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: "Sukses", message: "Akun anda berhasil dibuat", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                            self.present(alertController, animated: true, completion: nil)
                        }
                        self.navigationController?.popViewController(animated: true)
                    }


                case .failure(let error):
                    print(error)
                }
            })
    }
    
    @IBAction func tfPasswordChange(_ sender: Any) {
            if let password = tfPass.text{
                if let errorMessage = invalidPassword(password){
                    labelRegexPass.text = errorMessage
                    labelRegexPass.isHidden = false
                } else {
                    labelRegexPass.isHidden = true
                }
            }
            checkValidationForm()
        }
        
        func invalidPassword(_ value: String) -> String?{
            if (value.count < 6){
                return "Password minimal 6 karakter"
            }
            if !containsDigit(value){
                return "Password harus mengandung angka"
            }
            if !containsLowerCase(value){
                return "Password harus mengandung huruf kecil"
            }
            if !containsUpperCase(value){
                return "Password harus mengandung huruf kapital"
            }
            
            return nil
        }
        
        func containsDigit(_ value: String) -> Bool{
            let regex = ".*[0-9].*"
            let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
            return predicate.evaluate(with: value)
        }
        
        func containsLowerCase(_ value: String) -> Bool{
            let regex = ".*[a-z].*"
            let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
            return predicate.evaluate(with: value)
        }
        
        func containsUpperCase(_ value: String) -> Bool{
            let regex = ".*[A-Z].*"
            let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
            return predicate.evaluate(with: value)
        }
    
    @IBAction func tfUsernameChange(_ sender: Any) {
        if let username = tfNoHP.text{
            if let errorMessage = invalidUsername(username){
                labelRegexUsername.text = errorMessage
                labelRegexUsername.isHidden = false
            } else {
                labelRegexUsername.isHidden = true
            }
        }
        checkValidationForm()
    }
    
    func invalidUsername(_ value: String) -> String?{
        if (value.count < 10){
            return "No. Handphone minimal 10 karakter"
        }
        if !containsNumber(value){
            return "No. Handphone harus berupa angka"
        }
        return nil
    }
    
    func invalidNamaLengkap(_ value: String) -> String? {
        if (value.count < 1){
            return "Nama lengkap harus diisi"
        }
        return nil
    }
    
    func invalidTanggalLahir(_ value: String) -> String? {
        if (value.count < 1){
            return "Tanggal lahir harus diisi"
        }
        return nil
    }
    
    func containsNumber(_ value: String) -> Bool{
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    @IBAction func tfEmailChange(_ sender: Any) {
        if let email = tfEmail.text{
            if let errorMessage = invalidEmail(email){
                labelRegexEmail.text = errorMessage
                labelRegexEmail.isHidden = false
            } else {
                labelRegexEmail.isHidden = true
            }
        }
        checkValidationForm()
    }
    
    func invalidEmail(_ value: String) -> String?{
        if !isValidEmail(value){
            return "Email tidak valid"
        }
        return nil
    }
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func checkValidationForm(){
        if labelRegexPass.isHidden && labelRegexUsername.isHidden && labelRegexRetypePass.isHidden && labelRegexEmail.isHidden && labelRegexNamaLengkap.isHidden && labelRegexTglLahir.isHidden {
            btnRegister.isEnabled = true
        } else {
            btnRegister.isEnabled = false
        }
    }
   
    @IBAction func cekPassword(_ sender: Any) {
            if let repeatPass = tfRetypePass.text {
                if let errorMessage = invalidRepeatPass(repeatPass) {
                    labelRegexRetypePass.text = errorMessage
                    labelRegexRetypePass.isHidden = false
                } else {
                    labelRegexRetypePass.isHidden = true
                }
            }
            checkValidationForm()
        }
        
        func invalidRepeatPass(_ value: String) -> String? {
            if !repeatPassword(value) {
                return "Password yang Anda masukkan berbeda"
            }
            return nil
        }
        
        func repeatPassword(_ value: String) -> Bool {
            let passwordText = tfPass.text
            let repeatPass = tfRetypePass.text
            if passwordText == repeatPass {
                return true
            }
            return false
        }
    
    
    

   
}
