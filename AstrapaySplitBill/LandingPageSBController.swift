//
//  LandingPageSBController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 09/12/22.
//

import UIKit

class LandingPageSBController: UIViewController {
   

    @IBOutlet weak var btnCreateBill: UIButton!
    @IBOutlet weak var btnBillHistory: UIButton!
    @IBOutlet weak var btnBackToHome: UIButton!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnCreateBill.addTarget(self, action: #selector(btnCreateBillTapped), for: .touchUpInside)
        btnCreateBill.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        btnCreateBill.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        btnCreateBill.layer.shadowOpacity = 1.0
        btnCreateBill.layer.shadowRadius = 0.0
        btnCreateBill.layer.masksToBounds = false
        btnCreateBill.layer.cornerRadius = 4.0
        
        btnBillHistory.addTarget(self, action: #selector(buttonBillHistoryTapped), for: .touchUpInside)
        btnBillHistory.layer.cornerRadius = 30
        btnBillHistory.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.2).cgColor
        btnBillHistory.layer.shadowOffset = CGSize(width: 0.0, height: 6.0)
        btnBillHistory.layer.shadowOpacity = 1.0
        btnBillHistory.layer.shadowRadius = 0.0
        btnBillHistory.layer.masksToBounds = false
        btnBillHistory.layer.cornerRadius = 4.0
        
        btnBackToHome.addTarget(self, action: #selector(btnBackToHomeTapped), for: .touchUpInside)
        btnBackToHome.layer.borderColor = UIColor.blue.cgColor
        btnBackToHome.layer.borderWidth = 1
      
        
     
    }
    
    @objc func btnCreateBillTapped(){
            let createBillOptionsViewController = self.storyboard?.instantiateViewController(withIdentifier: "CreateBillOptionsViewController") as! CreateBillOptionsViewController
            self.navigationController?.pushViewController(createBillOptionsViewController, animated: true)
    }
    
    @objc func buttonBillHistoryTapped(){
            let historyBillViewController = self.storyboard?.instantiateViewController(withIdentifier: "HistoryBillViewController") as! HistoryBillViewController
            self.navigationController?.pushViewController(historyBillViewController, animated: true)
    }
    
    @objc func btnBackToHomeTapped() {
            let homeControllerr = self.storyboard?.instantiateViewController(withIdentifier: "HomeController") as! HomeController
            self.navigationController?.pushViewController(homeControllerr, animated: true)
    }

}
