//
//  InputManualController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 12/12/22.
//

import UIKit
import DropDown

class InputManualController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate  {
    
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var txtInput2: UITextField!
    @IBOutlet weak var txtInput3: UITextField!
    @IBOutlet weak var txtInput4: UITextField!
    @IBOutlet weak var txtInput5: UITextField!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnCreateSB: UIButton!
    @IBOutlet weak var btnDetails: UIButton!
    @IBOutlet weak var btnDropDown: UIButton!
    
    @IBOutlet weak var lblDropDown: UILabel!
    @IBOutlet weak var lblTax: UILabel!
    
    @IBOutlet weak var btnDeleteAll: UIButton!
    
    var id: Int = 0
    var arrContact = [String]()
    var savedContact = [String]()
    var txtArr = [String]()
    
    var rootSplit = RootSplit()
    var split = [Split]()
    var tempSplit = Split()
    
    var arrSplit = RootSplit()
    
    let dropDown = DropDown()
    let choiceValues = ["Bagi Sama Rata", "Bagi Sesuai Pesanan"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        btnCreateSB.addTarget(self, action: #selector(btnCreateSBTapped), for: .touchUpInside)
        btnCreateSB.layer.cornerRadius = 5
        btnCreateSB.isEnabled = false
        
        btnDetails.addTarget(self, action: #selector(btnDetailsTapped), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(btnBackTapped), for: .touchUpInside)
        btnDeleteAll.addTarget(self, action: #selector(btnDeleteAllTapped), for: .touchUpInside)
    
        txtInput.autocorrectionType = .no
        txtInput2.autocorrectionType = .no
        txtInput3.autocorrectionType = .no
        txtInput4.autocorrectionType = .no
        
        btnAdd.isEnabled = false;
        txtInput.addTarget(self, action:  #selector(textFieldInputDidChange(_:)),  for:.editingChanged )
        txtInput2.addTarget(self, action:  #selector(textFieldInputDidChange(_:)),  for:.editingChanged )
        txtInput3.addTarget(self, action:  #selector(textFieldInputDidChange(_:)),  for:.editingChanged )
        
        assignBackground()
        
        lblDropDown.text = "Pilih Tipe Pembagian"
        dropDown.anchorView = btnDropDown
        dropDown.dataSource = choiceValues
        dropDown.bottomOffset = CGPoint(x: 0, y: (dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y: -(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.direction = .bottom
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Tipe pembagian yang dipilih: \(item) at index: \(index)")
            self.lblDropDown.text = choiceValues[index]
        }
        
        let c = arrContact.joined(separator:", ")
        txtInput4.text = c
        
        validForm()
        
        if UserDefaults.standard.array(forKey: "person") != nil {
            print("tidak nol")
            let strPerson = UserDefaults.standard.stringArray(forKey: "person") ?? [String]()
            print(strPerson)
            savedContact = strPerson
            
            for contact in arrContact {
                if savedContact.contains(contact) {
                    continue
                }
                savedContact.append(contact)
            }
            
            UserDefaults.standard.set(savedContact, forKey: "person")
            UserDefaults.standard.synchronize()
            
            let updatedPerson = UserDefaults.standard.stringArray(forKey: "person") ?? [String]()
            print(updatedPerson)
            print(updatedPerson.count)
        } else {
            print("nol")
            print(arrContact)
            
            if arrContact.isEmpty {
                print("kosong")
            } else {
                UserDefaults.standard.set(arrContact, forKey: "person")
                UserDefaults.standard.synchronize()
            }
        }
        
        if UserDefaults.standard.object(forKey: "mySplit") != nil {
            if let data = UserDefaults.standard.object(forKey: "mySplit") as? Data,
                let mySplit = try? JSONDecoder().decode(RootSplit.self, from: data) {
                arrSplit = mySplit
                print(arrSplit)
            }
        }
    }
    
    
    func validForm() {
        txtInput5.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        guard txtInput5.isValid() else {
            print("empty")
            btnCreateSB.isEnabled = false
            return
        }

        checkValid()
    }
    
    @objc func textFieldInputDidChange(_ sender: UITextField) {
        if txtInput.text == "" || txtInput2.text == "" || txtInput3.text == ""  {
                btnAdd.isEnabled = false;
            }else{
                btnAdd.isEnabled = true;
            }
        }
    
    func checkValid() {
        btnCreateSB.isEnabled = true
    }
    
    func assignBackground() {
       let background = UIImage(named: "bg-sb-input")

       var imageView : UIImageView!
       imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
       imageView.clipsToBounds = true
       imageView.image = background
       imageView.center = view.center
       view.addSubview(imageView)
       self.view.sendSubviewToBack(imageView)
        
    }
    
    
    
    @objc func btnDeleteAllTapped() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if key == "login_token" {
                return
            }
            if key == "saved_username" {
                return
            }
            defaults.removeObject(forKey: key)
        }
        
        let inputManualController = self.storyboard?.instantiateViewController(withIdentifier: "InputManualController") as! InputManualController
        self.navigationController?.pushViewController(inputManualController, animated: true)
    }
    
    @objc func btnBackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnCreateSBTapped() {
        let tax = txtInput5.text
        let convertTax = Double(tax!)
        
        let confirmationPageViewController = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationPageViewController") as! ConfirmationPageViewController
        confirmationPageViewController.tax = convertTax!
        confirmationPageViewController.totalItem = arrSplit.split.count
        self.navigationController?.pushViewController(confirmationPageViewController, animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    @objc func btnDetailsTapped() {
        let contactsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
        contactsViewController.id = 1
        self.navigationController?.pushViewController(contactsViewController, animated: true)
    }
    
    @IBAction func btnDropDownTapped(_ sender: Any) {
        dropDown.show()
    }

    @IBAction func onClickAddButton(_ sender: Any) {
        if let txt = txtInput.text,
           let txt2 = txtInput2.text,
           let txt3 = txtInput3.text,
           let txt4 = txtInput4.text,
           let choice = lblDropDown.text
        {
            let txtCombine = txt + "-" + txt2 + "-" + txt3 + "-" + txt4
            txtArr = txtCombine.components(separatedBy: "-")
            
            let username = UserDefaults.standard.string(forKey: "saved_username")
            
            tempSplit.itemName = txtArr[0]
            tempSplit.qty = Int(txtArr[2])
            tempSplit.price = Double(txtArr[1])
            tempSplit.person = arrContact
            
            var arrPrice = [Double]()
            var result: Double = 0
            for contact in arrContact {
                if UserDefaults.standard.array(forKey: "\(contact)") != nil {
                    print("ada")
                    print(contact)
                    let data = UserDefaults.standard.array(forKey: "\(contact)") as? [Double] ?? [Double]()
                    print(data)
                    arrPrice = data
                    
                    if choice == choiceValues[0] {
                        result = tempSplit.price! / Double(tempSplit.qty!)
                        
                        arrPrice.append(result)
                    } else {
                        arrPrice.append(tempSplit.price!)
                    }
                    
                    UserDefaults.standard.set(arrPrice, forKey: "\(contact)")
                    UserDefaults.standard.synchronize()
                    
                    print(result)
                    print(arrPrice)
                } else {
                    print("gaada")
                    print(contact)
                    
                    if choice == choiceValues[0] {
                        result = tempSplit.price! / Double(tempSplit.qty!)
                        
                        arrPrice.append(result)
                    } else {
                        arrPrice.append(tempSplit.price!)
                    }
                    
                    print(result)
                    print(arrPrice)
                    
                    UserDefaults.standard.set(arrPrice, forKey: "\(contact)")
                    UserDefaults.standard.synchronize()
                }
                
                arrPrice.removeAll()
                result = 0
            }
            
            if UserDefaults.standard.object(forKey: "mySplit") != nil {
                if let data = UserDefaults.standard.object(forKey: "mySplit") as? Data,
                    let mySplit = try? JSONDecoder().decode(RootSplit.self, from: data) {
                    print(mySplit)
                    arrSplit = mySplit
                    arrSplit.addSplit(s: tempSplit)
                    
                    if let encoded = try? JSONEncoder().encode(arrSplit) {
                        UserDefaults.standard.set(encoded, forKey: "mySplit")
                        UserDefaults.standard.synchronize()
                    }
                }
            } else {
                rootSplit.user = username
                rootSplit.addSplit(s: tempSplit)
                
                if let encoded = try? JSONEncoder().encode(rootSplit) {
                    UserDefaults.standard.set(encoded, forKey: "mySplit")
                    UserDefaults.standard.synchronize()
                }
                
                if let data = UserDefaults.standard.object(forKey: "mySplit") as? Data, let mySplit = try? JSONDecoder().decode(RootSplit.self, from: data) {
                        arrSplit = mySplit
                }
            }
            
            let inputManualController = self.storyboard?.instantiateViewController(withIdentifier: "InputManualController") as! InputManualController
            self.navigationController?.pushViewController(inputManualController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSplit.split.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditTableViewCell", for: indexPath)
        
        let splitItem = arrSplit.split[indexPath.row]
        
        let labelItem = cell.viewWithTag(1) as! UILabel
        labelItem.text = splitItem?.itemName ?? ""
        
        let labelQty = cell.viewWithTag(2) as! UILabel
        
        let walletInt = Int(splitItem?.price ?? 0)
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: walletInt as NSNumber) {
            labelQty.text = "Rp " + formattedTipAmount
        }
        
        let labelHarga = cell.viewWithTag(3) as! UILabel
        labelHarga.text = "\(splitItem?.qty ?? 0)"
        
        let labelPerson = cell.viewWithTag(4) as! UILabel
        let p = splitItem?.person.joined(separator: ", ")
        labelPerson.text = p
        
        return cell

    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: "Delete") {
            (action, view, completionHandler) in
            self.arrSplit.split.remove(at: indexPath.row)
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            
            completionHandler(true)
                                  
        }
        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

struct RootSplit: Codable {
    var user: String?
    var split = [Split?]()
    
    mutating func addSplit(s: Split) {
        split.append(s)
    }
}

struct Split: Codable {
    var itemName: String?
    var qty: Int?
    var price: Double?
    var person = [String]()
}

extension UITextField {
    func isValid() -> Bool {
        guard let text = self.text,
              !text.isEmpty else {
            return false
        }

        return true
    }
}
