//
//  CreateBillOptionsViewController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 17/12/22.
//

import UIKit

class CreateBillOptionsViewController: UIViewController {
    
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var lblCreateBill: UILabel!
    
    @IBOutlet weak var viewInputManual: UIView!
    @IBOutlet weak var imgInputManual: UIImageView!
    @IBOutlet weak var lblInputManual: UILabel!
    @IBOutlet weak var descInputManual: UILabel!
    @IBOutlet weak var btnInputManual: UIButton!
    
    @IBOutlet weak var viewUploadImage: UIView!
    @IBOutlet weak var imgUploadImage: UIImageView!
    @IBOutlet weak var lblUploadImage: UILabel!
    @IBOutlet weak var descUploadImage: UILabel!
    @IBOutlet weak var btnUploadImage: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblInputManual.text = "Input Manual"
        descInputManual.text = "Masukkan item secara manual dan tentukan jumlah tagihan per orang"
        
        lblUploadImage.text = "Unggah Gambar"
        descUploadImage.text = "Unggah gambar total tagihan dan bagikan kepada teman atau keluarga"
        
        btnBack.addTarget(self, action: #selector(btnBackTapped), for: .touchUpInside)
        btnInputManual.addTarget(self, action: #selector(btnInputManualTapped), for: .touchUpInside)
        btnUploadImage.addTarget(self, action: #selector(btnUploadImageTapped), for: .touchUpInside)
        
        viewInputManual.layer.borderColor = UIColor.lightGray.cgColor
        viewInputManual.layer.borderWidth = 0.5
        viewInputManual.layer.cornerRadius = 20
        viewInputManual.layer.shadowColor = UIColor.darkGray.cgColor
        viewInputManual.layer.shadowOpacity = 0.5
        viewInputManual.layer.shadowOffset = CGSize.zero
        viewInputManual.layer.shadowRadius = 20
        
        viewUploadImage.layer.borderColor = UIColor.lightGray.cgColor
        viewUploadImage.layer.borderWidth = 0.5
        viewUploadImage.layer.cornerRadius = 20
        viewUploadImage.layer.shadowColor = UIColor.darkGray.cgColor
        viewUploadImage.layer.shadowOpacity = 0.5
        viewUploadImage.layer.shadowOffset = CGSize.zero
        viewUploadImage.layer.shadowRadius = 20

        assignbackground()
    }
    
    func assignbackground(){
           let background = UIImage(named: "bg-sb-options")

           var imageView : UIImageView!
           imageView = UIImageView(frame: view.bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
           imageView.clipsToBounds = true
           imageView.image = background
           imageView.center = view.center
           view.addSubview(imageView)
           self.view.sendSubviewToBack(imageView)
        
    }
    
    @objc func btnBackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnInputManualTapped() {
        let inputManualController = self.storyboard?.instantiateViewController(withIdentifier: "InputManualController") as! InputManualController
        self.navigationController?.pushViewController(inputManualController, animated: true)
    }
    
    @objc func btnUploadImageTapped() {
        let scanImageViewController = self.storyboard?.instantiateViewController(withIdentifier: "ScanImageViewController") as! ScanImageViewController
        self.navigationController?.pushViewController(scanImageViewController, animated: true)
    }

  

}
