//
//  SettingController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 07/12/22.
//

import UIKit
import Alamofire

class SettingController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tvSetting: UITableView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var viewHeader: UIView!
    
    @IBOutlet weak var lblNamaLengkap: UILabel!
    @IBOutlet weak var lblNoHandphone: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        tvSetting.delegate = self
        tvSetting.dataSource = self
        
        btnBack.addTarget(self, action: #selector(buttonBackTapped), for: .touchUpInside)
        
        getCurrentUser()
    }
        
    override func viewDidAppear(_ animated: Bool) {
        getCurrentUser()
    }
    
    @objc func buttonBackTapped() {
        self.dismiss(animated: true)
    }
    
    @objc func buttonUbahProfilTapped() {
        let editProfilController = self.storyboard?.instantiateViewController(withIdentifier: "EditProfilController")
        editProfilController?.modalPresentationStyle = .overCurrentContext
        self.present(editProfilController!, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath)
            
            _ = cell.viewWithTag(7) as! UIImageView
            
            let lblMember = cell.viewWithTag(8) as! UILabel
            lblMember.text = "Preferred Member"
            
            return cell
        } else if indexPath.section == 1 {
           
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath)
            
            let btnUbahProfil = cell.viewWithTag(9) as! UIButton
            btnUbahProfil.addTarget(self, action: #selector(buttonUbahProfilTapped), for: .touchUpInside)
            
            let lblUbahProfil = cell.viewWithTag(10) as! UILabel
            lblUbahProfil.text = "Ubah Profil"
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell4", for: indexPath)
            
            let btnKeluar = cell.viewWithTag(11) as! UIButton
            btnKeluar.addTarget(self, action: #selector(buttonKeluarTapped), for: .touchUpInside)
            
            _ = cell.viewWithTag(12) as! UILabel
            

            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @objc func buttonKeluarTapped() {
        
        let alertController = UIAlertController(title: "Keluar", message: "Apakah anda yakin untuk keluar?", preferredStyle: .actionSheet);
                    
                    alertController.addAction(UIAlertAction(title: "Keluar", style: .default, handler: {action in  UserDefaults.standard.set("unset", forKey: "login_token")
                        let nav = self.presentingViewController as? UINavigationController
                        
                        self.dismiss(animated: true) {
                            let _ = nav?.popToRootViewController(animated: true)
                        }}));
                    
                    alertController.addAction(UIAlertAction(title: "Batal", style: .default, handler: nil));
                    self.present(alertController, animated: true, completion: nil)
    }
    
    func getCurrentUser() {
        let token = UserDefaults.standard.string(forKey: "login_token")!
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token)"]

        AF.request("http://localhost:8080/api/current-user/", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            switch response.result {
            case .success:
                let itemObject = response.value as? [String : Any]
                let status = itemObject?["status"] as? Int
                let data = itemObject?["data"] as? [String: Any]
                let userDet = data?["userDet"] as? [String: Any]
                
                self.lblNamaLengkap.text = userDet?["fullName"] as? String
                self.lblNoHandphone.text = data?["username"] as? String

                //UserDefaults.standard.set(token, forKey: "login_token")

                if status ?? 0 == 401 {
                    UserDefaults.standard.set("unset", forKey: "login_token")
                    UserDefaults.standard.removeObject(forKey: "saved_username")

                    let homeController = self.storyboard?.instantiateViewController(withIdentifier: "HomeController") as! HomeController
                    self.navigationController?.pushViewController(homeController, animated: true)
                }
            case .failure(let error):
                print(error)
            }
        })
    }

    
}
