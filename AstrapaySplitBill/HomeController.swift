//
//  HomeController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 06/12/22.
//

import UIKit
import Alamofire

class HomeController: UIViewController, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let token = UserDefaults.standard.string(forKey: "login_token")
    
    @IBOutlet weak var tvDashboard: UITableView!
    
    var username = ""
    var data = ""
    var collectionPromoInfo: UICollectionView!
    var collectionPenawaran: UICollectionView!
    var imgPromo = [
        "https://www.astrapay.com/static-assets/images/upload/promo-mobile/thumbnail/undianbengkel_general_1000x434.png",
        "https://www.astrapay.com/static-assets/images/upload/promo-mobile/thumbnail/Maumodal%20app%20banner%20AP-01.jpg",
        "https://www.astrapay.com/static-assets/images/upload/promo-mobile/thumbnail/ULTRAJUMBO-AP-2-04.jpeg"
    ]
    
    var imgPenawaran = [
            "https://maucash.id/wp-content/uploads/2022/10/Maumodal-app-banner-AP-03.jpg",
            "https://maucash.id/wp-content/uploads/2022/10/Astrapay-November-22-02-scaled.jpg",
            "https://astrapay.com/static-assets/images/upload/blog/main/2022/10/fraudawareness_onlineshop_blog903x903.png",
            "https://astrapay.com/static-assets/images/upload/blog/main/2022/07/externalfraud_juli_gantiPIN_1080x1080.png"
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        tvDashboard.delegate = self
        tvDashboard.dataSource = self
        
        getCurrentUser()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getCurrentUser()
    }
    
    @objc func buttonSettingTapped() {
        let settingController = self.storyboard?.instantiateViewController(withIdentifier: "SettingController")
        settingController?.modalPresentationStyle = .overCurrentContext
        self.present(settingController!, animated: true)
    }
    
    @objc func buttonSplitBillTapped(){
        let landingPageSBController = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageSBController") as! LandingPageSBController
        self.navigationController?.pushViewController(landingPageSBController, animated: true)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
            
            let btnSetting = cell.viewWithTag(1) as! UIButton
            btnSetting.addTarget(self, action: #selector(buttonSettingTapped), for: .touchUpInside)
            
            let btnRefresh = cell.viewWithTag(2) as! UIButton
            btnRefresh.addTarget(self, action: #selector(btnRefreshTapped), for: .touchUpInside)
            
            let imgQRIS = cell.viewWithTag(3) as! UIImageView
            imgQRIS.image = UIImage(named: "qris")
            
            
            let lblWelcome = cell.viewWithTag(4) as! UILabel
            lblWelcome.text = "Halo, "
            let lblWelcomeNama = cell.viewWithTag(38) as! UILabel
            lblWelcomeNama.text = username
            
            let cardView = cell.viewWithTag(5)!
            cardView.layer.cornerRadius = 14
            
            let viewPreffered = cell.viewWithTag(6)!
            viewPreffered.layer.cornerRadius = 12
            
            let subCardView = cell.viewWithTag(7)!
            subCardView.layer.cornerRadius = 14
            subCardView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            
            _ = cell.viewWithTag(8) as! UIImageView
//            imgMenuCard1.image = UIImage(named: "qrisIcon")
            
            let lblMenuCard1 = cell.viewWithTag(9) as! UILabel
            lblMenuCard1.text = "Scan"
            
            _ = cell.viewWithTag(10) as! UIImageView
//            imgMenuCard2.image = UIImage(named: "TopupIcon")
            
            let lblMenuCard2 = cell.viewWithTag(11) as! UILabel
            lblMenuCard2.text = "Top Up"
            
            _ = cell.viewWithTag(12) as! UIImageView
//            imgMenuCard3.image = UIImage(named: "TransferIcon")
            
            let lblMenuCard3 = cell.viewWithTag(13) as! UILabel
            lblMenuCard3.text = "Transfer"
            
            _ = cell.viewWithTag(14) as! UIImageView
//            imgMenuCard4.image = UIImage(named: "RiwayatIcon")
            
            let lblMenuCard4 = cell.viewWithTag(15) as! UILabel
            lblMenuCard4.text = "Riwayat"
            
            return cell
        } else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell2", for: indexPath)
            _ = cell.viewWithTag(16)!
            
            _ = cell.viewWithTag(17) as! UIButton
            
            let labelEmail = cell.viewWithTag(18) as! UILabel
            labelEmail.text = "Verifikasi email kamu sekarang"
            
            return cell
        } else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell3", for: indexPath)
            
            let imgAngsuran = cell.viewWithTag(19) as! UIImageView
            imgAngsuran.image = UIImage(named: "Angsuran")
            
            let lblAngsuran = cell.viewWithTag(20) as! UILabel
            lblAngsuran.text = "Angsuran"
            
            let imgPulsa = cell.viewWithTag(21) as! UIImageView
            imgPulsa.image = UIImage(named: "Pulsa")
            
            let lblPulsa = cell.viewWithTag(22) as! UILabel
            lblPulsa.text = "Pulsa"
            
            let imgPaketData = cell.viewWithTag(23) as! UIImageView
            imgPaketData.image = UIImage(named: "Paket Data")
            
            let lblPaketData = cell.viewWithTag(24) as! UILabel
            lblPaketData.text = "Paket Data"
            
            let imgPLN = cell.viewWithTag(25) as! UIImageView
            imgPLN.image = UIImage(named: "PLN")
            
            let lblPLN = cell.viewWithTag(26) as! UILabel
            lblPLN.text = "PLN"
            
            let imgBills = cell.viewWithTag(45) as! UIImageView
            imgBills.image = UIImage(named: "bill")
            
            
            let imgTelkom = cell.viewWithTag(27) as! UIImageView
            imgTelkom.image = UIImage(named: "Telkom")
            let lblTelkom = cell.viewWithTag(28) as! UILabel
            lblTelkom.text = "Telkom"
            
            let btnSplitBill = cell.viewWithTag(29) as! UIButton
            btnSplitBill.addTarget(self, action: #selector(buttonSplitBillTapped), for: .touchUpInside)
            
            let lblBills = cell.viewWithTag(30) as! UILabel
            lblBills.text = "Split Bill"
            
            let imgAsuransi = cell.viewWithTag(31) as! UIImageView
            imgAsuransi.image = UIImage(named: "Asuransi")
            let lblAsuransi = cell.viewWithTag(32) as! UILabel
            lblAsuransi.text = "Asuransi"
            
            let imgLainnya = cell.viewWithTag(33) as! UIImageView
            imgLainnya.image = UIImage(named: "Lainnya")
            
            let lblLainnya = cell.viewWithTag(34) as! UILabel
            lblLainnya.text = "Lainnya"
            
            return cell
            
        } else if indexPath.section == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell4", for: indexPath)

            let lblTitle = cell.viewWithTag(35) as! UILabel
            lblTitle.text = "Promosi & Info"

            collectionPromoInfo = (cell.viewWithTag(36) as! UICollectionView)

            collectionPromoInfo.delegate = self
            collectionPromoInfo.dataSource = self

            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell5", for: indexPath)
                        
            let lblTitle = cell.viewWithTag(40) as! UILabel
            lblTitle.text = "Produk & Penawaran"

            collectionPenawaran = cell.viewWithTag(38) as! UICollectionView

            collectionPenawaran.delegate = self
            collectionPenawaran.dataSource = self
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return 360
        } else if indexPath.section == 1 {
            return 70
        } else if indexPath.section == 2{
            return 230
        } else {
            return 220
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == self.collectionPromoInfo{
            return 3
        } else {
            return 4
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == self.collectionPromoInfo{
            return 1
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.collectionPromoInfo {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell4", for: indexPath)
            
            let img = imgPromo[indexPath.row]
            
            let imageCollection = cell.viewWithTag(37) as! UIImageView
            imageCollection.kf.setImage(with: URL(string: img))
           
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell5", for: indexPath)
                        
            var img = imgPenawaran[indexPath.row]
            
            let imageCollection = cell.viewWithTag(39) as! UIImageView
            imageCollection.kf.setImage(with: URL(string: img))
          
            
            return cell
        }
       
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == self.collectionPromoInfo{
            return CGSize.init(width: 340, height: 180)
        } else {
            return CGSize.init(width: 160, height: 190)
        }
        
    }
    
    @objc func btnRefreshTapped() {
        let homeController = self.storyboard?.instantiateViewController(withIdentifier: "HomeController") as! HomeController
        self.navigationController?.pushViewController(homeController, animated: false)
    }
    
    func getCurrentUser() {
        let token = UserDefaults.standard.object(forKey: "login_token")
        let headers: HTTPHeaders = ["Authorization": "Bearer \(token ?? "")"]

        AF.request("http://localhost:8080/api/current-user/", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            switch response.result {
            case .success:
                let itemObject = response.value as? [String : Any]
                let status = itemObject?["status"] as? Int
                let data = itemObject?["data"] as? [String: Any]
                let userDet = data?["userDet"] as? [String: Any]
                
                if status ?? 0 == 401 {
                    UserDefaults.standard.set("unset", forKey: "login_token")
                    UserDefaults.standard.removeObject(forKey: "saved_username")

                    let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                    self.navigationController?.pushViewController(viewController, animated: true)
                } else {
                    self.username = userDet?["fullName"] as! String
                    print(self.username)

                    self.tvDashboard.reloadData()
                }
            case .failure(let error):
                print(error)
            }
        })
    }
}
