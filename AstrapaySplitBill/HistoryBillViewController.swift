//
//  HistoryBillViewController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 13/12/22.
//

import UIKit
import Alamofire
import Kingfisher

class HistoryBillViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var data: [[String: Any]] = []
    var dataAmount: [[String: Any]] = []
    var totalAmount: Int64 = 0
    

    @IBOutlet weak var viewDown: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var lblTotalPengeluaran: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewDown.layer.cornerRadius = 70
        
        btnBack.addTarget(self, action: #selector(btnBackTapped), for: .touchUpInside)
        
        tableView.separatorColor = UIColor(white: 0.95, alpha: 1)
        tableView.dataSource = self
        tableView.delegate = self
        
        assignBackground()
        getHistory()
        getTotalExpenses()
    }
    
    @objc func btnBackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnDetailTapped() {
        
    }
    
    func assignBackground() {
           let background = UIImage(named: "background")

           var imageView : UIImageView!
           imageView = UIImageView(frame: view.bounds)
           imageView.contentMode =  UIView.ContentMode.scaleAspectFill
           imageView.clipsToBounds = true
           imageView.image = background
           imageView.center = view.center
           view.addSubview(imageView)
           self.view.sendSubviewToBack(imageView)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let itemData = data[indexPath.row]
        
        print(" row \(indexPath.row) data \(itemData)")
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.contentView.backgroundColor = UIColor(white: 0.95, alpha: 1)
        
        let totalParticipants = itemData["totalParticipants"] as! Int
        let totalItem = itemData["totalItem"] as! Int
        let dateSplitBill = itemData["dateSplitBill"] as! String
        let timeSplitBill = itemData["timeSplitBill"] as! String
        
        let lblTglBill = cell.viewWithTag(2) as! UILabel
        lblTglBill.text = (itemData["dateSplitBill"] as! String)
        let lblJamBill = cell.viewWithTag(3) as! UILabel
        lblJamBill.text = (itemData["timeSplitBill"] as! String)
        let lblTotalItems = cell.viewWithTag(4) as! UILabel
        lblTotalItems.text = "Total item \(itemData["totalItem"] as! Int)"
        let lblTotalPerson = cell.viewWithTag(5) as! UILabel
        lblTotalPerson.text = "Total Partisipan \(itemData["totalParticipants"] as! Int)"
        
        let btnRincian = cell.viewWithTag(6) as! UIButton
        btnRincian.addTarget(self, action: #selector(btnRincianTapped), for: .touchUpInside)
        
        return cell
    }
    
    @objc func btnRincianTapped(_ sender: UIButton) {
        var superView = sender.superview
        while let view = superView, !(view is UITableViewCell) {
            superView = view.superview
        }
        guard let cell = superView as? UITableViewCell else {
            return
        }
        
        guard let indexPath = tableView.indexPath(for: cell) else {
            return
        }
        let itemData = data[indexPath.row]
        let id_history = itemData["idHistory"] as! Int64
        
        
        let historyDetailsViewController = self.storyboard?.instantiateViewController(withIdentifier: "HistoryDetailsViewController") as! HistoryDetailsViewController
        historyDetailsViewController.idHistory = id_history
        self.navigationController?.pushViewController(historyDetailsViewController, animated: true)

    }
    
//    func getHistoryDetails() {
//         let token = UserDefaults.standard.object(forKey: "login_token")
//         let headers : HTTPHeaders = [
//                     "Authorization": "Bearer \(token ?? "")",
//                     "Accept": "application/json",
//                     "Content-Type": "application/json" ]
//         AF.request("http://localhost:8080/api/current-history/\(idHistory)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
//             switch response.result {
//                 case .success:
//                 let itemObject = response.value as! [String : Any]
//                 let dataSingle = itemObject["data"] as! [String: Any]
//                 let dataHisDet = dataSingle["historyDet"] as! [[String: Any]]
//                 for dets in dataHisDet {
//                     let amount = dets["amount"] as! Int64
//                     self.totalAmount += amount
//                 }
//                 self.lblTotalPengeluaran.text = String(self.totalAmount)
//
//
//
//
//                 case .failure(let error):
//                     print(error)
//                 }
//             })
//     }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
   func getHistory() {
        let token = UserDefaults.standard.object(forKey: "login_token")
                let headers : HTTPHeaders = [
                            "Authorization": "Bearer \(token ?? "")",
                            "Accept": "application/json",
                            "Content-Type": "application/json" ]
        AF.request("http://localhost:8080/api/current-history/", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            switch response.result {
                case .success:
                let itemObject = response.value as! [String : Any]
                self.data = itemObject["data"] as! [[String: Any]]
               
                self.tableView.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
    
    func getTotalExpenses() {
        var totalExpanses: Int64 = 0
        let token = UserDefaults.standard.object(forKey: "login_token")
        let headers : HTTPHeaders = [
                    "Authorization": "Bearer \(token ?? "")",
                    "Accept": "application/json",
                    "Content-Type": "application/json" ]
        AF.request("http://localhost:8080/api/total-expenses/", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
            switch response.result {
                case .success:
                let itemObject2 = response.value as! [String : Any]
                let messaage = itemObject2["message"] as! String
                print(itemObject2)
                let data = itemObject2["data"] as! [[String: Any]]
                print("ini print")
                print("Data Amount: \(data)")
                
                for expanses in data {
                    let amount = expanses["amount"] as! Double
                    totalExpanses += Int64(amount)
                    
                    let walletInt = Int(totalExpanses)
                    let formatter = NumberFormatter()
                    formatter.locale = Locale(identifier: "id_ID")
                    formatter.groupingSeparator = "."
                    formatter.numberStyle = .decimal
                    if let formattedTipAmount = formatter.string(from: walletInt as! NSNumber) {
                        self.lblTotalPengeluaran.text = "Rp " + formattedTipAmount
                    }
                    
                }
               
                
                

                self.tableView.reloadData()
                    
                case .failure(let error):
                    print(error)
                }
            })
    }
}
