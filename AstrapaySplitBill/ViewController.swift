//
//  ViewController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 06/12/22.
//

import UIKit
import Alamofire

class ViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var logoAP: UIImageView!
    @IBOutlet weak var labelUsername: UILabel!
    @IBOutlet weak var tfUsername: UITextField!
    @IBOutlet weak var labelPassword: UILabel!
    @IBOutlet weak var tfPassword: UITextField!
   
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var labelRegexPass: UILabel!
    @IBOutlet weak var labelRegexUsername: UILabel!
    
    var iconClick = false
    var username = ""
    var password = ""

    
    let imageIcon = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelUsername.text = "Masukkan No. Handphone"
        labelPassword.text = "Masukkan Password"
        
        logoAP.image = UIImage(named: "astrapay")
        
        tfUsername.delegate = self
        tfPassword.delegate = self
        
       
        btnRegister.addTarget(self, action: #selector(btnRegisterTapped), for: .touchUpInside)
        btnLogin.addTarget(self, action: #selector(btnLoginTapped), for: .touchUpInside)
        
        labelRegexPass.isHidden = true
        labelRegexUsername.isHidden = true
        btnLogin.isEnabled = false
        
        imageIcon.image = UIImage(named: "hide")
        
        let contentView = UIView()
        contentView.addSubview(imageIcon)
        
        contentView.frame = CGRect(x: 0, y: 0, width: UIImage(named: "hide")!.size.width, height: UIImage(named: "hide")!.size.height)
        
        imageIcon.frame = CGRect(x: -10, y: 0, width: UIImage(named: "hide")!.size.width, height: UIImage(named: "hide")!.size.height)
        
        tfPassword.rightView = contentView
        tfPassword.rightViewMode = .always
        
        let dismissGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.handleTapDismiss(_:)))
        self.view.addGestureRecognizer(dismissGesture)
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageIcon.isUserInteractionEnabled = true
        imageIcon.addGestureRecognizer(tapGestureRecognizer)
        
        isLogin()
    }
    
    @objc func imageTapped(tapGestureRecognizer:UITapGestureRecognizer) {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        if iconClick {
            iconClick = false
            tappedImage.image = UIImage(named: "view")
            tfPassword.isSecureTextEntry = false
        } else {
            iconClick = true
            tappedImage.image = UIImage(named: "hide")
            tfPassword.isSecureTextEntry = true
        }
    }
    
   
    
    @objc func btnRegisterTapped() {
        let registerController = self.storyboard?.instantiateViewController(withIdentifier: "RegisterController") as! RegisterController

        self.navigationController?.pushViewController(registerController, animated: true)
    }
    
    @objc func btnLoginTapped() {
        let param = ["username": tfUsername.text,"password": tfPassword.text]

        AF.request("http://localhost:8080/login", method: .post, parameters: param, encoding: JSONEncoding.default, headers: nil).responseJSON(completionHandler: { response in
            switch response.result {
            case .success:
                print(response.value!)
                let itemObject = response.value as? [String : Any]
                let data = itemObject?["data"] as? [String: Any]

                let token = data?["token"] as? String
                let username = data?["username"] as? String
                let status = itemObject? ["status"] as? Int
                
                if status != 401 {
                    UserDefaults.standard.set(token, forKey: "login_token")
                    UserDefaults.standard.set(username, forKey: "saved_username")
                    
                    self.dismiss(animated: true)
                    
                    let homeController = self.storyboard?.instantiateViewController(withIdentifier: "HomeController") as! HomeController
                    self.navigationController?.pushViewController(homeController, animated: true)
                } else {
                    let alert = UIAlertController(title: "Login gagal", message: "Username atau password yang Anda masukkan tidak valid", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            case .failure(let error):
                let alert = UIAlertController(title: "Login gagal", message: "Username atau password yang Anda masukkan tidak valid", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                print(error)
            }
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.endEditing(true)
        return true
    }
    
    @IBAction func tfUsernameChange(_ sender: Any) {
        if let username = tfUsername.text{
            if let errorMessage = invalidUsername(username){
                labelRegexUsername.text = errorMessage
                labelRegexUsername.isHidden = false
            } else {
                labelRegexUsername.isHidden = true
            }
        }
        checkValidationForm()
    }
    
    func invalidUsername(_ value: String) -> String?{
        if (value.count < 10){
            return "No. Handphone minimal 10 karakter"
        }
        if !containsNumber(value){
            return "No. Handphone harus berupa angka"
        }
        return nil
    }
    
    func containsNumber(_ value: String) -> Bool{
        let regex = "^[0-9]*$"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    @IBAction func tfPasswordChange(_ sender: Any) {
            if let password = tfPassword.text{
                if let errorMessage = invalidPassword(password){
                    labelRegexPass.text = errorMessage
                    labelRegexPass.isHidden = false
                } else {
                    labelRegexPass.isHidden = true
                }
            }
            checkValidationForm()
        }
        
    func invalidPassword(_ value: String) -> String?{
        if (value.count < 6){
            return "Password minimal 6 karakter"
        }
        if !containsDigit(value){
            return "Password harus mengandung angka"
        }
        if !containsLowerCase(value){
            return "Password harus mengandung huruf kecil"
        }
        if !containsUpperCase(value){
            return "Password harus mengandung huruf kapital"
        }
        
        return nil
    }
        
    func containsDigit(_ value: String) -> Bool{
        let regex = ".*[0-9].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func containsLowerCase(_ value: String) -> Bool{
        let regex = ".*[a-z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
        
    func containsUpperCase(_ value: String) -> Bool{
        let regex = ".*[A-Z].*"
        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: value)
    }
    
    func checkValidationForm(){
        if labelRegexPass.isHidden && labelRegexUsername.isHidden {
            btnLogin.isEnabled = true
        } else {
            btnLogin.isEnabled = false
        }
    }
    
    func isLogin() {
        let token = UserDefaults.standard.string(forKey: "login_token")
        print(token!)
        
        if token != "unset" {
            let homeController = self.storyboard?.instantiateViewController(withIdentifier: "HomeController") as! HomeController

            self.navigationController?.pushViewController(homeController, animated: true)
        }
    }
    
    @objc func handleTapDismiss(_ sender: UITapGestureRecognizer? = nil) {
        self.view.endEditing(true)
    }
}

