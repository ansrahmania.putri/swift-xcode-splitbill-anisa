//
//  EditProfilController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 07/12/22.
//

import UIKit
import Alamofire

class EditProfilController: UIViewController {
    @IBOutlet weak var viewEdit: UIView!
    
    @IBOutlet weak var tfNamaLengkap: UITextField!
    
    @IBOutlet weak var labelNamaLengkap: UILabel!
    @IBOutlet weak var labelTglLahir: UILabel!
    @IBOutlet weak var tfTglLahir: UITextField!
    
    @IBOutlet weak var btnSaveUbahProfil: UIButton!
    @IBOutlet weak var btnBatalUbahProfil: UIButton!
    
    
    let datePicker = UIDatePicker()
    
    let token = UserDefaults.standard.string(forKey: "login_token")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showDatePicker()
        
        btnSaveUbahProfil.addTarget(self, action: #selector(btnSaveUbahProfilTapped), for: .touchUpInside)
        btnBatalUbahProfil.addTarget(self, action: #selector(btnBatalUbahProfilTapped), for: .touchUpInside)
        
        tfNamaLengkap.autocorrectionType = .no
        tfTglLahir.autocorrectionType = .no

    }
    
    func showDatePicker(){
      datePicker.datePickerMode = .date
        datePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels

     let toolbar = UIToolbar();
     toolbar.sizeToFit()
     let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
    let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));

   toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

    tfTglLahir.inputAccessoryView = toolbar
    tfTglLahir.inputView = datePicker

   }

    @objc func donedatePicker(){

     let formatter = DateFormatter()
     formatter.dateFormat = "yyyy-MM-dd"
     tfTglLahir.text = formatter.string(from: datePicker.date)
     self.view.endEditing(true)
   }

   @objc func cancelDatePicker(){
      self.view.endEditing(true)
       
   }
    
    @objc func btnBatalUbahProfilTapped() {
        self.dismiss(animated: true)
    }
    
    @objc func btnSaveUbahProfilTapped() {
        let headers : HTTPHeaders = [
                    "Authorization": "Bearer \(token ?? "")"]
        let param = ["fullName": tfNamaLengkap.text,"birthDate": tfTglLahir.text]
        
        AF.request("http://localhost:8080/api/user/details/", method: .post, parameters: param, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
                switch response.result {
                case .success:
                    let itemObject = response.value as? [String : Any]
                    let data = itemObject?["data"] as? [[String: Any]]
//                    let username = data?["username"] as? String
//                    let password = data?["password"] as? String
//                    let email = data?["email"] as? String
//                    print(response.value)
                    
                    let success = itemObject? ["success"] as? Bool
                    let message = itemObject? ["message"] as? String
                    
                    print(itemObject)
                    
                    if success ?? false == true {
                        DispatchQueue.main.async {
                            let alertController = UIAlertController(title: "Success!", message: "Update profil berhasil", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                            self.present(alertController, animated: true, completion: nil)
                        }
                        self.dismiss(animated: false
                        )
                    }


                case .failure(let error):
                    print(error)
                }
            })
    }
 
}
    

   

