//
//  ContactsViewController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 13/12/22.
//

import UIKit
import Contacts
import ContactsUI


class ContactsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, CNContactPickerDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var id: Int = 0
    var contactStore = CNContactStore()
    var contacts = [ContactStruct]()
    var stringContacts = [String]()
    var items = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        contactStore.requestAccess(for: .contacts) {
            (success, errror) in
            if success {
                print("Authorization Successfull")
            }
        }
        fetchContacts()
        
        btnBack.addTarget(self, action: #selector(btnBackTapped), for: .touchUpInside)
        
    }
    
    @objc func btnBackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        let contactToDisplay = contacts[indexPath.row]
        cell.textLabel?.text = contactToDisplay.givenName + " " + contactToDisplay.familyName
        cell.detailTextLabel?.text = contactToDisplay.number
        
        //select contacts
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }
    
    func fetchContacts() {
        let key = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey] as [CNKeyDescriptor]
        let request = CNContactFetchRequest(keysToFetch: key)
        try! contactStore.enumerateContacts(with:
                                                request) {
            (contact, stoppingPointer) in
            let name = contact.givenName
            let familyName = contact.familyName
            let number = contact.phoneNumbers.first?.value.stringValue
            
            let contactToAppend = ContactStruct (givenName: name, familyName: familyName, number: number!)
            contacts.append(contactToAppend)
        }
        tableView.reloadData()
        for contact in contacts {
            stringContacts.append(contact.givenName)
            //        print(contacts.first?.givenName)
        }
    }
    
    
    @IBAction func btnDoneTapped(_ sender: Any) {
        items.removeAll()
        if let selectedContact = tableView.indexPathsForSelectedRows {
            for iPath in selectedContact {
                items.append(stringContacts[iPath.row])
            }
            print("----You have selected contacts----")
        }
        
        if id == 1 {
            let inputManualController = self.storyboard?.instantiateViewController(withIdentifier: "InputManualController") as! InputManualController
            inputManualController.arrContact = items
            self.navigationController?.pushViewController(inputManualController, animated: true)
        } else {
            let scanImageViewController = self.storyboard?.instantiateViewController(withIdentifier: "ScanImageViewController") as! ScanImageViewController
            scanImageViewController.arrContact = items
            self.navigationController?.pushViewController(scanImageViewController, animated: true)
        }
    }
}
