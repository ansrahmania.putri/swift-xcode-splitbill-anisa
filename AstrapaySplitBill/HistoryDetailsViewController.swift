//
//  HistoryDetailsViewController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 23/12/22.
//

import UIKit
import Alamofire

class HistoryDetailsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var dataHisDet: [[String: Any]] = []
    var idHistory: Int64 = 0
    var idHistoryDet: Int64 = 0

    @IBOutlet var viewTengah: UIView!
    @IBOutlet weak var viewGambar: UIView!
    @IBOutlet weak var viewPutih: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var lblTanggal: UILabel!
    @IBOutlet weak var lblJam: UILabel!
    @IBOutlet weak var lblTotalPartisipan: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewPutih.clipsToBounds = true
        viewPutih.layer.cornerRadius = 60
        viewPutih.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        btnBack.addTarget(self, action: #selector(btnBackTapped), for: .touchUpInside)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        getHistoryDetails()
       
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataHisDet.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let itemData = dataHisDet[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        cell.viewWithTag(1) as! UIImageView
        let lblNama = cell.viewWithTag(2) as! UILabel
        lblNama.text = (itemData["contactName"] as! String)
        let lblTotalTagihan = cell.viewWithTag(3) as! UILabel
        
        var totalAmount = (itemData["amount"] as! Double)
        
        let walletInt = Int(totalAmount)
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: walletInt as NSNumber) {
            lblTotalTagihan.text = "Rp " + formattedTipAmount
        }
        

        let lblStatus = cell.viewWithTag(4) as! UILabel
        lblStatus.text = (itemData["paymentStatus"] as! String)
        self.idHistoryDet = itemData["idHistoryDet"] as! Int64
        
//        let btnKonfirmasiBayar = cell.viewWithTag(5) as! UIButton
//        btnKonfirmasiBayar.addTarget(self, action: #selector(btnKonfirmasiBayarTapped), for: .touchUpInside)
        
        return cell
    }
    
    @objc func btnBackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
//    @objc func btnKonfirmasiBayarTapped() {
//        let token = UserDefaults.standard.object(forKey: "login_token")
//        let headers : HTTPHeaders = [
//                    "Authorization": "Bearer \(token ?? "")",
//                    "Accept": "application/json",
//                    "Content-Type": "application/json" ]
//        print(idHistoryDet)
//        AF.request("http://localhost:8080/api/current-history/\(idHistoryDet)/update-details/", method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
//            switch response.result {
//                case .success:
//                let itemObject = response.value as! [String : Any]
//                self.dataHisDet = itemObject["data"] as! [[String: Any]]
//                print("Hasil update \(itemObject)")
//
//                self.tableView.reloadData()
//
//                case .failure(let error):
//                    print(error)
//                }
//            })
//    }
    
    func getHistoryDetails() {
         let token = UserDefaults.standard.object(forKey: "login_token")
         let headers : HTTPHeaders = [
                     "Authorization": "Bearer \(token ?? "")",
                     "Accept": "application/json",
                     "Content-Type": "application/json" ]
         AF.request("http://localhost:8080/api/current-history/\(idHistory)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON(completionHandler: { response in
             switch response.result {
                 case .success:
                 let itemObject = response.value as! [String : Any]
                 let dataSingle = itemObject["data"] as! [String: Any]
                 self.dataHisDet = dataSingle["historyDet"] as! [[String: Any]]
                
                 
                 
                 self.lblTanggal.text = (dataSingle["dateSplitBill"] as! String)
                 self.lblJam.text = (dataSingle["timeSplitBill"] as! String)
                 self.lblTotalPartisipan.text = "\((dataSingle["totalParticipants"] as! Int))"
                 
                 print("\(self.dataHisDet)")
                
                 self.tableView.reloadData()
                     
                 case .failure(let error):
                     print(error)
                 }
             })
     }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }

}
