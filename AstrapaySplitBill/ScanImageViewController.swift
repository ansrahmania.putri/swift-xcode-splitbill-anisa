//
//  ScanImageViewController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 16/12/22.
//

import UIKit
import Vision
import DropDown

class ScanImageViewController: UIViewController {

    @IBOutlet weak var viewUnggahGambar: UIView!
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnKontak: UIButton!
    @IBOutlet weak var btnDeleteAll: UIButton!
    
    @IBOutlet weak var btnPilihGambar: UIButton!
    @IBOutlet weak var btnKonfirmasi: UIButton!
    
    @IBOutlet weak var textKontak: UITextField!
    
    @IBOutlet weak var imageView: UIImageView!
    
    var id: Int = 0
    var arrContact = [String]()
    var request = VNRecognizeTextRequest(completionHandler: nil)
    
    let dropDown = DropDown()
    let choiceValuesTipe = ["Bagi Sama Rata"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewUnggahGambar.clipsToBounds = true
        viewUnggahGambar.layer.cornerRadius = 60
        viewUnggahGambar.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        btnBack.addTarget(self, action: #selector(btnBackTapped), for: .touchUpInside)
        btnKontak.addTarget(self, action: #selector(btnKontakTapped), for: .touchUpInside)
        btnDeleteAll.addTarget(self, action: #selector(btnDeleteAllTapped), for: .touchUpInside)
        btnKonfirmasi.addTarget(self, action: #selector(btnKonfirmasiTapped), for: .touchUpInside)
        
        btnPilihGambar.layer.cornerRadius = 5
        
        
        let c = arrContact.joined(separator:", ")
        textKontak.text = c
        
        if arrContact.isEmpty {
            print("kosong")
        } else {
            UserDefaults.standard.set(arrContact, forKey: "person")
            UserDefaults.standard.synchronize()
        }
    }
    
    @objc func btnKontakTapped() {
        let contactsViewController = self.storyboard?.instantiateViewController(withIdentifier: "ContactsViewController") as! ContactsViewController
        contactsViewController.id = 2
        self.navigationController?.pushViewController(contactsViewController, animated: true)
    }
   
    @objc func btnBackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnKonfirmasiTapped() {
        var arrPrice = [Double]()
        var result: Double = 0
        
        let hargaStruk = UserDefaults.standard.string(forKey: "hasil_image")
        let h = hargaStruk?.replacingOccurrences(of: ".", with: "")
        
        for contact in arrContact {
            print(contact)
            
            result = Double(h!)! / Double(arrContact.count)
            
            arrPrice.append(result)
            
            print(result)
            print(arrPrice)
            
            UserDefaults.standard.set(arrPrice, forKey: "\(contact)")
            UserDefaults.standard.synchronize()
            
            arrPrice.removeAll()
            result = 0
        }
        
        let confirmationUploadImageController = self.storyboard?.instantiateViewController(withIdentifier: "ConfirmationUploadImageController") as! ConfirmationUploadImageController
        self.navigationController?.pushViewController(confirmationUploadImageController, animated: true)
    }
    
    @objc func btnDeleteAllTapped() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if key == "login_token" {
                return
            }
            if key == "saved_username" {
                return
            }
            defaults.removeObject(forKey: key)
        }
        
        let scanImageViewController = self.storyboard?.instantiateViewController(withIdentifier: "ScanImageViewController") as! ScanImageViewController
        self.navigationController?.pushViewController(scanImageViewController, animated: true)
    }
    
    @IBAction func touchUpInsideCameraButton(_ sender: Any) {
        setupGallery()
    }
    
    
 
    
    private func setupGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePhotoLibraryPicker = UIImagePickerController()
            imagePhotoLibraryPicker.delegate = self
            imagePhotoLibraryPicker.allowsEditing = true
            imagePhotoLibraryPicker.sourceType = .photoLibrary
            self.present(imagePhotoLibraryPicker, animated: true, completion: nil)
        }
    }
    
    private func setupVisionTextRecognizeImage(image: UIImage?) {
        request = VNRecognizeTextRequest(completionHandler: {(request, error) in
            
            guard let observations = request.results as? [VNRecognizedTextObservation] else {
                fatalError("Received invalid observation")}
            
                for observation in observations {
                    guard let topCandidate = observation.topCandidates(1).first else {
                        print("No candidate")
                        continue
                    }
                    
                    print("hasil top candidate \(topCandidate)")
                    
                    let textString = "\(topCandidate.string)"
                    
                    DispatchQueue.main.async {
                        UserDefaults.standard.set(textString, forKey: "hasil_image")
                        print(textString)
                    }
                }
        })
        
        request.customWords = ["cust0m"]
        request.minimumTextHeight = 0.03125
        request.recognitionLevel = .accurate
        request.recognitionLanguages = ["en_US"]
        
        let requests = [request]
    
        DispatchQueue.global(qos: .userInitiated).async {
            guard let img = image?.cgImage else {
                fatalError("Missing image to scan")}
                let handle = VNImageRequestHandler(cgImage: img, options: [:])
                try?handle.perform(requests)
            
        }
    }
}

extension ScanImageViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        picker.dismiss(animated: true, completion: nil)
        
        let image = info[UIImagePickerController.InfoKey.originalImage]as? UIImage
        self.imageView.image = image
        
        setupVisionTextRecognizeImage(image: image)
    }
    
}


