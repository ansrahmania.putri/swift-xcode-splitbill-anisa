//
//  LandingPageConfirmationController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 15/12/22.
//

import UIKit

class LandingPageConfirmationController: UIViewController {
    @IBOutlet weak var imgSuccess: UIImageView!
    @IBOutlet weak var lblSuccess: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnDone: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnDone.addTarget(self, action: #selector(btnDoneTapped), for: .touchUpInside)
        btnDone.layer.borderColor = UIColor.blue.cgColor
        btnDone.layer.borderWidth = 1
    }
    
    @objc func btnDoneTapped() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if key == "login_token" {
                return
            }
            if key == "saved_username" {
                return
            }
            defaults.removeObject(forKey: key)
        }
        
        let landingPageSBController = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageSBController") as! LandingPageSBController
        self.navigationController?.pushViewController(landingPageSBController, animated: true)
    }
}
