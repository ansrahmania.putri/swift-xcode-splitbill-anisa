//
//  ConfirmationUploadImageController.swift
//  AstrapaySplitBill
//
//  Created by ADA-NB187 on 23/12/22.
//

import UIKit
import Alamofire

class ConfirmationUploadImageController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var subViewHeader: UIView!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSendSplitBill: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalPartisipan: UILabel!
    @IBOutlet weak var totalHarga: UILabel!
    
    var namePerson = [String]()
    var totalTagihan = [Double]()
    
    var his = His()
    var det = [HisDet?]()
    var tempDet = HisDet()
     
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self

        let hargaStruk = UserDefaults.standard.string(forKey: "hasil_image")
        totalHarga.text = hargaStruk
        
        let strPerson = UserDefaults.standard.stringArray(forKey: "person") ?? [String]()
        namePerson = strPerson
        
        btnSendSplitBill.addTarget(self, action: #selector(btnSplit), for: .touchUpInside)
        btnBack.addTarget(self, action: #selector(btnBackTapped), for: .touchUpInside)
        
        totalPartisipan.text = String(namePerson.count)
    }
    
    @objc func btnBackTapped() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return namePerson.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let person = namePerson[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath)
        
        _ = cell.viewWithTag(1) as! UIImageView
        let lblNama = cell.viewWithTag(2) as! UILabel
        lblNama.text = person
        
        let data = UserDefaults.standard.array(forKey: "\(person)") as? [Double] ?? [Double]()
        totalTagihan = data
        
        var t: Double = 0
        for total in totalTagihan {
            t = t + total
        }
        let tString = String(format: "%.0f",ceil(t * 100)/100)
        
        
        let lblTotal = cell.viewWithTag(3) as! UILabel
        
        
        let walletInt = Int(tString)
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "id_ID")
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        if let formattedTipAmount = formatter.string(from: walletInt as! NSNumber) {
        lblTotal.text = "Rp " + formattedTipAmount
        }
    
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    @objc func btnSplit() {
        for person in namePerson {
            tempDet.contactName = person
            
            let data = UserDefaults.standard.array(forKey: "\(person)") as? [Double] ?? [Double]()
            totalTagihan = data
            
            var t: Double = 0
            for total in totalTagihan {
                t = t + total
            }
            tempDet.amount = t
            his.addHisDet(s: tempDet)
        }
        print(his)
        
        det = his.historyDet
        
        do {
            let jsonData = try JSONEncoder().encode(det)
            let jsonString = String(data: jsonData, encoding: .utf8)!
            let data = jsonString.data(using: .utf8)
            
            do {
                if let jsonArray = try JSONSerialization.jsonObject(with: data!, options : .allowFragments) as? [Dictionary<String,Any>]
                {
                   print(jsonArray)
                    
                    let param = [
                        "totalParticipants": totalPartisipan.text!,
                        "totalItem": 1,
                        "historyDet": jsonArray
                    ] as [String : Any]

                    print(param)
                    
                    let token = UserDefaults.standard.string(forKey: "login_token")
                    let headers : HTTPHeaders = [
                                "Authorization": "Bearer \(token ?? "")"]

                    AF.request("http://localhost:8080/api/history/", method: .post, parameters: param, encoding: JSONEncoding.prettyPrinted, headers: headers).responseJSON(completionHandler: { response in
                        switch response.result {
                        case .success:
                            
                            let landingPageConfirmationController = self.storyboard?.instantiateViewController(withIdentifier: "LandingPageConfirmationController") as! LandingPageConfirmationController
                            self.navigationController?.pushViewController(landingPageConfirmationController, animated: true)

                            
                        case .failure(let error):
                            print(error)
                        }
                    })
                } else {
                    print("bad json")
                }
            } catch let error as NSError {
                print(error)
            }
        } catch { print(error) }
    }

}
